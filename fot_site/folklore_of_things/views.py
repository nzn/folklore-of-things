from django.http import HttpResponse
from django.shortcuts import render
from .models import Things


def index(request):
    things_list = Things.objects.all()
    context = {'things_list': things_list}
    return render(request, 'index.html', context)

