from django.apps import AppConfig


class FolkloreOfThingsConfig(AppConfig):
    name = 'folklore_of_things'
