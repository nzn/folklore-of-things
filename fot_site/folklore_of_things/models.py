from django.db import models

class Things(models.Model):
    thing = models.CharField(max_length=200)
    key = models.TextField(max_length=256, unique=True)
    description = models.TextField()