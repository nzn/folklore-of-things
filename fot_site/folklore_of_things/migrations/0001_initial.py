# Generated by Django 2.2.6 on 2019-10-27 21:08

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Things',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('thing', models.CharField(max_length=200)),
                ('key', models.TextField(max_length=256, unique=True)),
                ('description', models.TextField()),
            ],
        ),
    ]
